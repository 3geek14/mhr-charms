# Monster Hunter Rise Charm odds

Have you ever wondered what the odds are of getting a better charm in Monster
Hunter Rise? Do you want to know whether you should be using Wisp of Mystery,
Rebirth, or maybe even Moonbow? This project exists to help you answer those
questions.

Select which skills you want in your build from the list, including the maximum
number of points you want (if lower than what can appear on charms). Tell the
tool how many points you're hoping for, and it'll tell you the odds for each
melding method.

Because of the flexibility in armour pieces and the flexibility that comes from
all the decorations we can craft, the most important thing in a charm is how
many "points" it gives you. Every point in a skill that has at least a level 2
decoration and every slot you can fit one of those decorations into counts as a
point. For example, a god roll AB3/WEX2 3-1-1 charm is 6 points. Subpoints are
what this tool uses to refer to points in skills that occupy level 1 slots.
In general, those skills are much less important than the level 2 and 3 skills,
so an AB3 charm with no slots is usually preferred over a Speed Sharpening 3/AB2
1-1-1 charm.

This tool is still under construction. Once it's ready, this README will be
updated with a link to the tool.
