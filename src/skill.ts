import { q, BigAmount } from 'big-amount'
import { MeldMethod } from './melding'

export enum Grade {
    Null = -1,
    C = 0,
    B = 1,
    A = 2,
    S = 3,
}

export class Skill {
    static readonly Null: Skill = new Skill(
        '',
        Grade.Null,
        0,
        [
            [[100], [100], [100]],
            [[100], [100], [100]],
            [[100], [100], [100]],
            [[100], [100], [100]],
            [[100], [100], [100]],
            [[100], [100], [100]],
            [[100], [100], [100]],
        ],
        [0, 0, 0, 0, 0, 0, 0]
    )

    odds: BigAmount[][][] // 5 meld method, 3 tables, 7 levels
    pickRate: BigAmount[]
    constructor(
        public name: string,
        public grade: Grade,
        public slot: number,
        odds: number[][][],
        pickRate?: number[]
    ) {
        const sums = odds.map((method) =>
            method.map((table) => table.reduce((sum, x) => sum + x, 0))
        )
        this.odds = odds.map((method, i) =>
            method.map((table, j) =>
                table.map((level) =>
                    sums[i][j] === 0 ? q(0, 1) : q(level, sums[i][j])
                )
            )
        )
        this.pickRate = pickRate?.map((x) => q(x, 100)) ?? [
            q(0, 1),
            q(0, 1),
            q(0, 1),
            q(0, 1),
            q(0, 1),
            q(0, 1),
            q(0, 1),
        ]
    }

    canBeNthSkill(method: MeldMethod, skillSlot: number) {
        return this.odds[method][skillSlot].some((val) => !val.eq(q(0, 1)))
    }

    canBeTarget(method: MeldMethod) {
        return this.pickRate[method].gt(q(0, 1))
    }

    equals(other: Skill): boolean {
        for (let i = 0; i < 5; ++i) {
            for (let j = 0; j < 3; ++j) {
                const thisOdd = this.odds[i][j]
                const otherOdd = other.odds[i][j]
                if (thisOdd.length !== otherOdd.length) {
                    return false
                }
                for (let k = 0; k < thisOdd.length; ++k) {
                    if (!thisOdd[k].eq(otherOdd[k])) {
                        return false
                    }
                }
            }
        }
        return this.name === other.name && this.grade === other.grade
    }
}

export class GenericSkill extends Skill {
    constructor(
        grade: Grade,
        public quantity: number,
        first: boolean,
        second: boolean,
        third: boolean
    ) {
        super('', grade, 0, [], [])
        const odds1 = first ? [q(1, 1)] : [q(0, 1)]
        const odds2 = second ? [q(1, 1)] : [q(0, 1)]
        const odds3 = third ? [q(1, 1)] : [q(0, 1)]
        const odds = [odds1, odds2, odds3]
        this.odds = [odds, odds, odds, odds, odds, odds, odds]
    }
}
