// import React from 'react'
// import ReactDOM from 'react-dom'
// import logo from './logo.svg'
import './App.css'
import * as AllSkills from './allSkills'
import { compute, MeldMethod } from './melding'
import { Skill } from './skill'
import { q } from 'big-amount'

function App() {
    let someText: string | undefined = undefined
    return (
        <div className="App">
            <header className="App-header">
                {/* <img src={logo} className="App-logo" alt="logo" /> */}
                <p>
                    To contribute to this project, visit{' '}
                    <a href="https://gitlab.com/3geek14/mhr-charms/">
                        the repo on GitLab
                    </a>
                    .
                </p>
                <form
                    id="theForm"
                    onSubmit={(event) => {
                        event.preventDefault()
                        const target: EventTarget = event.target
                        // @ts-ignore
                        const method: MeldMethod = target[0].selectedIndex
                        const skillNames: string[] = [
                            // @ts-ignore
                            ...target[2].selectedOptions,
                        ].map((option) => option.value)
                        const targetSkillName: string =
                            // @ts-ignore
                            target[1].selectedOptions[0].value

                        const skills: Skill[] = skillNames.map(
                            (skillName) =>
                                AllSkills.AllSkills.filter(
                                    (skill) =>
                                        skill.name.localeCompare(skillName) ===
                                        0
                                )[0]
                        )
                        const targetSkill: Skill = AllSkills.AllSkills.filter(
                            (skill) =>
                                skill.name.localeCompare(targetSkillName) === 0
                        )[0]

                        let someText: string = ''
                        if (method < MeldMethod.Wisp) {
                            someText = compute(method, skills, targetSkill)
                        } else {
                            // eslint-disable-next-line @typescript-eslint/no-unused-vars
                            someText = compute(method, skills)
                        }

                        // console.log(target)
                        // console.log(event)
                    }}
                >
                    <label htmlFor="method">Choose a meld method:</label>{' '}
                    <select id="method">
                        <option value="0">Reflecting Pool</option>
                        <option value="1">Haze</option>
                        <option value="2">Moonbow</option>
                        <option value="3" selected>
                            Wisp of Mystery
                        </option>
                        <option value="4">Rebirth</option>
                        <option value="5">Anima</option>
                        <option value="6">Reincarnation</option>
                    </select>
                    <br />
                    <label htmlFor="target">
                        Which skill are you targeting?
                    </label>
                    <select id="target">
                        {AllSkills.AllSkills.filter((skill) =>
                            skill.pickRate.some((rate) => rate.gt(q(0, 1)))
                        )
                            .sort((a, b) => a.name.localeCompare(b.name))
                            .map((skill) => (
                                <option value={skill.name}>{skill.name}</option>
                            ))}
                    </select>
                    <br />
                    <label htmlFor="skills">
                        Select the skill(s) you care about:
                    </label>{' '}
                    <br />
                    <select id="skills" size={10} multiple>
                        {AllSkills.AllSkills.filter((skill) => skill.slot > 1)
                            .sort((a, b) => a.name.localeCompare(b.name))
                            .map((skill) => (
                                <option value={skill.name}>{skill.name}</option>
                            ))}
                    </select>
                    <br />
                    <input
                        type="submit"
                        id="submitButton"
                        value="Compute Odds!"
                    />
                </form>
                <div>
                    Results:
                    <br />
                    {someText || '(check the JS Console in your browser)'}
                </div>
            </header>
        </div>
    )
}

export default App
