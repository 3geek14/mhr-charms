export class BigRational {
    numerator: bigint
    denominator: bigint

    constructor(
        num: bigint | number | string | BigRational = 0n,
        denom: bigint | number | string | BigRational = 1n
    ) {
        let n: bigint = 1n,
            d: bigint = 1n

        if (typeof num === 'bigint') {
            n *= num
        } else if (typeof num === 'number') {
            let x = fromNumber(num)
            n *= x.numerator
            d *= x.denominator
        } else if (typeof num === 'string') {
            let x = fromString(num)
            n *= x.numerator
            d *= x.denominator
        } else {
            n *= num.numerator
            d *= num.denominator
        }

        if (typeof denom === 'bigint') {
            d *= denom
        } else if (typeof denom === 'number') {
            let x = fromNumber(denom)
            d *= x.numerator
            n *= x.denominator
        } else if (typeof denom === 'string') {
            let x = fromString(denom)
            d *= x.numerator
            n *= x.denominator
        } else {
            d *= denom.numerator
            n *= denom.denominator
        }

        let divisor = gcd(n, d)
        this.numerator = n / divisor
        this.denominator = d / divisor
    }
}

function fromNumber(x: number): BigRational {
    let denom = 1n
    while (x % 1 !== 0) {
        x *= 2
        denom *= 2n
    }
    let num = BigInt(x.toString(10))
    return new BigRational(num, denom)
}

function fromString(x: string): BigRational {
    throw new Error('Function not implemented.')
}

function gcd(n: bigint, d: bigint): bigint {
    throw new Error('Function not implemented.')
}
