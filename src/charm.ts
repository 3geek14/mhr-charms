/* eslint-disable @typescript-eslint/no-unused-vars */
import { BigAmount } from 'big-amount'
import {
    AllSkills,
    AttackBoost,
    CriticalEye,
    EvadeExtender,
    QuickSheath,
} from './allSkills'
import { Skill } from './skill'

export class Charm {
    constructor(
        public skill1: Skill,
        public level1: number,
        public skill2: Skill,
        public level2: number,
        public slots: number[]
    ) {}

    points(
        skills: Skill[]
    ): [level1: number, level2: number, level3: number, noDeco: number] {
        const points: [
            level1: number,
            level2: number,
            level3: number,
            noDeco: number
        ] = [0, 0, 0, 0]
        this.slots.forEach((slot) => {
            if (slot > 0) points[slot - 1]++
        })
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const limitingSkill = CriticalEye
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const limitingLevel = 3
        if (skills.includes(this.skill1)) {
            // if (this.skill1.equals(limitingSkill)) {
            //     points[this.skill1.slot - 1] += Math.min(this.level1, limitingLevel)
            // } else {
            //     points[this.skill1.slot - 1] += this.level1
            // }
            points[this.skill1.slot - 1] += this.level1
        }
        if (skills.includes(this.skill2)) {
            // if (this.skill2.equals(limitingSkill)) {
            //     points[this.skill2.slot - 1] += Math.min(this.level2, limitingLevel)
            // } else {
            //     points[this.skill2.slot - 1] += this.level2
            // }
            points[this.skill2.slot - 1] += this.level2
        }
        return points
    }

    toString(): string {
        return `${this.skill1.name} ${this.level1}, ${this.skill2.name} ${
            this.level2
        } (${this.slots.join('-')})`
    }

    toCSV(): string {
        return `${this.skill1.name},${this.level1},${this.skill2.name},${
            this.level2
        },${this.slots.join('-')}`
    }
}

export class CharmRoll {
    constructor(public charm: Charm, public odds: BigAmount) {}

    toString(): string {
        return `${this.charm.toString()}: ${this.odds.reduce().toString()}`
    }

    toCSV(): string {
        return `${this.charm.toCSV()},${this.odds.reduce().toString()}`
    }
}
