import { BigAmount, q } from 'big-amount'
import { Charm, CharmRoll } from './charm'
import { GenericSkill, Grade, Skill } from './skill'
import { AllSkills } from './allSkills'

export enum MeldMethod {
    Reflecting = 0, // Reflecting Pool
    Haze,
    Moonbow,
    Wisp, // Wisp of Mystery
    Rebirth,
    Anima,
    Reincarnation,
}

function everyCharm(method: MeldMethod, target?: Skill): CharmRoll[] {
    const canTarget = method <= MeldMethod.Moonbow
    if (canTarget && target === undefined) {
        throw new Error(
            'Method requires target skill, but no target specified.'
        )
    }
    const targetSkill = target as Skill
    if (canTarget && targetSkill.pickRate[method].eq(q(0, 1))) {
        console.log(
            targetSkill.name +
                " can't be the target for " +
                nameOfMethod(method)
        )
        return []
    }

    const getTargetOptions: boolean[] = canTarget ? [true, false] : [false]

    const charmRolls: CharmRoll[] = []

    getTargetOptions.forEach((getTarget) => {
        const firstSkillTable = canTarget && !getTarget ? 1 : 0
        const chanceOfGetTarget = canTarget
            ? getTarget
                ? targetSkill.pickRate[method]
                : q(1, 1).sub(targetSkill.pickRate[method])
            : q(1, 1)
        grades.forEach((grade1) => {
            if (getTarget && grade1 !== targetSkill.grade) return
            const chanceOfFirstGrade = getTarget
                ? q(1, 1)
                : skillGradeRates[method][firstSkillTable][grade1]
            const skillsInGrade1 = getTarget
                ? [targetSkill]
                : skillsByGrade[grade1].filter((skill) =>
                      skill.canBeNthSkill(method, firstSkillTable)
                  )
            skillsInGrade1.forEach((skill1) => {
                const chanceOfFirstSkillInGrade = q(1, skillsInGrade1.length)
                const gradesOrNull = [Grade.Null, ...grades]
                gradesOrNull.forEach((grade2) => {
                    const chanceOfSecondGrade =
                        grade2 === Grade.Null
                            ? q(1, 1).sub(rateOfSecondSkill[method][grade1])
                            : rateOfSecondSkill[method][grade1].mul(
                                  skillGradeRates[method][2][grade2]
                              )
                    const skillsInGrade2 =
                        grade2 === Grade.Null
                            ? [Skill.Null]
                            : skillsByGrade[grade2]
                                  .filter((skill) =>
                                      skill.canBeNthSkill(method, 2)
                                  )
                                  .filter((skill) => !skill.equals(skill1))
                    skillsInGrade2.forEach((skill2) => {
                        const chanceOfSecondSkillInGrade = q(
                            1,
                            skillsInGrade2.length
                        )
                        const slot1Rates = slotOdds(1, skill1, skill2, method)
                        const slot2Rates = slotOdds(2, skill1, skill2, method)
                        const slot3Rates = slotOdds(3, skill1, skill2, method)
                        for (
                            let level1 = 1;
                            level1 <=
                            skill1.odds[method][firstSkillTable].length;
                            ++level1
                        ) {
                            const chanceOfFirstLevel =
                                skill1.odds[method][firstSkillTable][level1 - 1]
                            for (
                                let level2 = 1;
                                level2 <= skill2.odds[method][2].length;
                                ++level2
                            ) {
                                const chanceOfSecondLevel =
                                    skill2.odds[method][2][level2 - 1]
                                for (
                                    let slot1 = 0;
                                    slot1 < slot1Rates.length;
                                    ++slot1
                                ) {
                                    const chanceOfSlot1 = slot1Rates[slot1]
                                    for (
                                        let slot2 = 0;
                                        slot2 < slot2Rates.length;
                                        ++slot2
                                    ) {
                                        const chanceOfSlot2 = slot2Rates[slot2]
                                        for (
                                            let slot3 = 0;
                                            slot3 < slot3Rates.length;
                                            ++slot3
                                        ) {
                                            const chanceOfSlot3 =
                                                slot3Rates[slot3]
                                            const chance = chanceOfGetTarget
                                                .mul(chanceOfFirstGrade)
                                                .mul(chanceOfFirstSkillInGrade)
                                                .mul(chanceOfSecondGrade)
                                                .mul(chanceOfSecondSkillInGrade)
                                                .mul(chanceOfFirstLevel)
                                                .mul(chanceOfSecondLevel)
                                                .mul(chanceOfSlot1)
                                                .mul(chanceOfSlot2)
                                                .mul(chanceOfSlot3)
                                            if (chance.eq(q(0, 1))) continue
                                            charmRolls.push(
                                                new CharmRoll(
                                                    new Charm(
                                                        skill1,
                                                        level1,
                                                        skill2,
                                                        level2,
                                                        [slot1, slot2, slot3]
                                                    ),
                                                    chance
                                                )
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    })
                })
            })
        })
    })

    return charmRolls
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function findCharms(method: MeldMethod, skills: Skill[]): CharmRoll[] {
    const badSkills: Skill[][] = [[], [], [], []]
    AllSkills.forEach((skill) => {
        if (skills.includes(skill)) return
        badSkills[skill.grade].push(skill)
    })
    const genericFirstNotSecond = badSkills
        .map((skillsInGrade) =>
            skillsInGrade
                .filter(
                    (skill) =>
                        skill.canBeNthSkill(method, 0) &&
                        !skill.canBeNthSkill(method, 2)
                )
                .reduce((count, skill) => count + 1, 0)
        )
        .map((count, idx) => new GenericSkill(idx, count, true, false, false))
    const genericSecondNotFirst = badSkills
        .map((skillsInGrade) =>
            skillsInGrade
                .filter(
                    (skill) =>
                        skill.canBeNthSkill(method, 2) &&
                        !skill.canBeNthSkill(method, 0)
                )
                .reduce((count, skill) => count + 1, 0)
        )
        .map((count, idx) => new GenericSkill(idx, count, false, true, true))
    const genericBoth = badSkills
        .map((skillsInGrade) =>
            skillsInGrade
                .filter(
                    (skill) =>
                        skill.canBeNthSkill(method, 0) &&
                        skill.canBeNthSkill(method, 2)
                )
                .reduce((count, skill) => count + 1, 0)
        )
        .map((count, idx) => new GenericSkill(idx, count, true, true, true))

    skills = [
        ...skills,
        ...genericFirstNotSecond,
        ...genericSecondNotFirst,
        ...genericBoth,
    ]
    const skillsOrNull: Skill[] = [...skills, Skill.Null]

    const skillGradeRates: BigAmount[][][] = [
        // 5 meld methods, 3 tables, 4 grades
        [
            [0, 0, 0, 0],
            [100, 0, 0, 0],
            [100, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0],
            [100, 0, 0, 0],
            [60, 25, 15, 0],
        ],
        [
            [0, 0, 0, 0],
            [50, 50, 0, 0],
            [60, 30, 10, 0],
        ],
        [
            [30, 52, 15, 3],
            [0, 0, 0, 0],
            [30, 52, 15, 3],
        ],
        [
            [25, 50, 20, 5],
            [0, 0, 0, 0],
            [25, 50, 20, 5],
        ],
    ].map((method) =>
        method.map((table) => table.map((grade) => q(grade, 100)))
    )
    const skillsPerGrade: number[][][] = [
        // 5 meld methods, 4 grades, 3 tables
        [
            // meld option 1
            [29, 11, 0],
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ],
        [
            // meld option 2
            [14, 15, 29],
            [23, 0, 12],
            [0, 0, 6],
            [0, 0, 0],
        ],
        [
            // Moonbow
            [0, 18, 29],
            [21, 7, 20],
            [4, 0, 10],
            [0, 0, 0],
        ],
        [
            // Wisp of Mystery
            [29, 0, 20],
            [30, 0, 30],
            [26, 0, 26],
            [16, 0, 16],
        ],
        [
            // Rebirth
            [21, 0, 14],
            [23, 0, 24],
            [26, 0, 26],
            [16, 0, 16],
        ],
    ]
    const rateOfSecondSkill: BigAmount[][] = [
        // 5 meld methods, 4 grades for first skill
        [0, 0, 0, 0],
        [60, 20, 0, 0],
        [80, 60, 30, 0],
        [100, 100, 20, 10],
    ].map((method) => method.map((grade) => q(grade, 100)))

    const charmRolls: CharmRoll[] = []

    skills.forEach((skill1) => {
        const chanceOfFirstGrade: BigAmount =
            skillGradeRates[method][0][skill1.grade]
        const chanceOfFirstSkillInGrade: BigAmount = q(
            (skill1 as GenericSkill).quantity === undefined
                ? 1
                : (skill1 as GenericSkill).quantity,
            skillsPerGrade[method][skill1.grade][0]
        )

        const chanceOfTwoSkills: BigAmount =
            rateOfSecondSkill[method][skill1.grade]
        for (
            let skill1Level = 1;
            skill1Level <= skill1.odds[method][0].length;
            skill1Level++
        ) {
            const chanceOfFirstLevel: BigAmount =
                skill1.odds[method][0][skill1Level - 1]
            skillsOrNull.forEach((skill2) => {
                if (skill1.equals(skill2)) return
                const oneSkill: boolean = skill2.equals(Skill.Null)

                const chanceOfSecondGrade: BigAmount = oneSkill
                    ? q(1, 1).sub(chanceOfTwoSkills)
                    : chanceOfTwoSkills.mul(
                          skillGradeRates[method][2][skill2.grade]
                      )
                const smallerSkillPool: boolean =
                    skill1.grade === skill2.grade &&
                    skill1.canBeNthSkill(method, 2)
                const chanceOfSecondSkillInGrade: BigAmount = oneSkill
                    ? q(1, 1)
                    : q(
                          (skill2 as GenericSkill).quantity === undefined
                              ? 1
                              : (skill2 as GenericSkill).quantity,
                          skillsPerGrade[method][skill2.grade][2] -
                              (smallerSkillPool ? 1 : 0)
                      )
                for (
                    let skill2Level = 1;
                    skill2Level <= skill2.odds[method][2].length;
                    skill2Level++
                ) {
                    const chanceOfSecondLevel: BigAmount =
                        skill2.odds[method][2][skill2Level - 1]
                    const s1Odds: BigAmount[] = slotOdds(
                        1,
                        skill1,
                        skill2,
                        method
                    )
                    const s2Odds: BigAmount[] = slotOdds(
                        2,
                        skill1,
                        skill2,
                        method
                    )
                    const s3Odds: BigAmount[] = slotOdds(
                        3,
                        skill1,
                        skill2,
                        method
                    )
                    for (
                        let slot1Level = 0;
                        slot1Level < s1Odds.length;
                        slot1Level++
                    ) {
                        const chanceOfSlot1: BigAmount = s1Odds[slot1Level]
                        for (
                            let slot2Level = 0;
                            slot2Level < s2Odds.length;
                            slot2Level++
                        ) {
                            const chanceOfSlot2: BigAmount = s2Odds[slot2Level]
                            for (
                                let slot3Level = 0;
                                slot3Level < s3Odds.length;
                                slot3Level++
                            ) {
                                const chanceOfSlot3: BigAmount =
                                    s3Odds[slot3Level]
                                const slots: number[] = [
                                    slot1Level,
                                    slot2Level,
                                    slot3Level,
                                ].filter((x) => x > 0)
                                const charm: Charm = new Charm(
                                    skill1,
                                    skill1Level,
                                    skill2,
                                    skill2Level,
                                    slots
                                )
                                const chance: BigAmount = chanceOfFirstGrade
                                    .mul(chanceOfFirstSkillInGrade)
                                    .mul(chanceOfTwoSkills)
                                    .mul(chanceOfFirstLevel)
                                    .mul(chanceOfSecondGrade)
                                    .mul(chanceOfSecondSkillInGrade)
                                    .mul(chanceOfSecondLevel)
                                    .mul(chanceOfSlot1)
                                    .mul(chanceOfSlot2)
                                    .mul(chanceOfSlot3)
                                    .reduce()
                                charmRolls.push(new CharmRoll(charm, chance))
                            }
                        }
                    }
                }
            })
        }
    })

    return charmRolls
}

export function compute(
    method: MeldMethod,
    skills: Skill[],
    target?: Skill
): string {
    // const charmRolls = findCharms(method, skills)
    const charmRolls = everyCharm(method, target)
    console.log(charmRolls.length)

    const methodName = nameOfMethod(method)
    let titleLog =
        'Using ' +
        methodName +
        (method < MeldMethod.Wisp
            ? ' targeting ' + (target as Skill).name
            : '') +
        ' looking for: '
    for (let i = 0; i < skills.length; ++i) {
        titleLog += skills[i].name
        if (i < skills.length - 1) {
            if (skills.length > 2) {
                titleLog += ','
            }
            titleLog += ' '
        }
        if (i === skills.length - 2) {
            titleLog += 'and '
        }
    }
    console.log(titleLog)

    let list: CharmRoll[] = []

    const rollsByPoints = charmRolls
        .reduce<{ point: number; rolls: CharmRoll[] }[]>(
            (grouped, roll) => {
                const pointsArray = roll.charm.points(skills)
                const points = pointsArray[1] + pointsArray[2] + pointsArray[3]
                // if (roll.charm.slots.every((slot, i) => slot === 3 - i)) {
                // if (roll.charm.slots.every((slot, i) => slot > 0)) {
                // if (roll.charm.skill2.equals(Skill.Null)) {
                // if (skills.includes(roll.charm.skill1)) {
                // if (pointsArray[1] + pointsArray[2] === 5) {
                grouped[points].rolls.push(roll)
                list.push(roll)
                // }
                // }
                return grouped
            },
            [
                { point: 0, rolls: [] },
                { point: 1, rolls: [] },
                { point: 2, rolls: [] },
                { point: 3, rolls: [] },
                { point: 4, rolls: [] },
                { point: 5, rolls: [] },
                { point: 6, rolls: [] },
                { point: 7, rolls: [] },
                { point: 8, rolls: [] },
                { point: 9, rolls: [] },
            ]
        )
        .filter((group) => group.rolls.length > 0)

    // list.sort((a, b) => a.odds.sub(b.odds).toNumber())
    // list.reverse()
    console.log(list.map((roll) => roll.toCSV()).join('\n'))

    const oddsForPoints = rollsByPoints.map((group) => ({
        point: group.point,
        odds: group.rolls.reduce(
            (sum, roll) => sum.add(roll.odds).reduce(),
            q(0, 1)
        ),
    }))
    const oddsForPointsOrBetter = oddsForPoints.map((group, i) => ({
        point: group.point,
        odds: oddsForPoints
            .slice(i)
            .reduce((sum, g) => sum.add(g.odds).reduce(), q(0, 1)),
    }))
    const level3SlotOdds = rollsByPoints.map((byPoint) => ({
        point: byPoint.point,
        odds: byPoint.rolls
            .filter((roll) => roll.charm.points(skills)[2] > 0)
            .reduce((sum, roll) => sum.add(roll.odds).reduce(), q(0, 1)),
    }))
    const oddsForLevel3SlotOrBetter = level3SlotOdds.map((group, i) => ({
        point: group.point,
        odds: level3SlotOdds
            .slice(i)
            .reduce((sum, g) => sum.add(g.odds).reduce(), q(0, 1)),
    }))
    // const level2SlotOdds = rollsByPoints
    //     .map(byPoint => ({
    //         point: byPoint.point,
    //         odds: byPoint.rolls
    //             .filter(roll => roll.charm.slots.some(x => x > 1))
    //             .reduce((sum, roll) => sum.add(roll.odds).reduce(), q(0, 1))
    //     }))
    // const oddsForLevel2SlotOrBetter = level2SlotOdds.map((group, i) => ({
    //     point: group.point,
    //     odds: level2SlotOdds
    //         .slice(i)
    //         .reduce((sum, g) => sum.add(g.odds).reduce(), q(0, 1))
    // }))

    let result: string = ''

    oddsForPointsOrBetter.forEach(({ point, odds }) => {
        const line = oddsToString(method, odds, point)
        console.log(line)
        result += line + '\n'
    })
    console.log('with a level 3 slot:')
    oddsForLevel3SlotOrBetter.forEach(({ point, odds }) => {
        const line = oddsToString(method, odds, point)
        console.log(line)
        result += line + '\n'
    })
    // console.log('with 3-1-1:')
    // console.log(oddsToString(method,
    //     charmRolls
    //         .filter(roll => {
    //             var points = roll.charm.points(skills)
    //             if (points[0] < 2) return false;
    //             if (points[2] < 3) return false;
    //             return true;
    //         })
    //         .reduce((sum, roll) => sum.add(roll.odds).reduce(), q(0, 1)),
    //     3, 2))

    return result
}

function nameOfMethod(method: MeldMethod) {
    switch (method) {
        case MeldMethod.Reflecting:
            return 'Reflecting Pool'
        case MeldMethod.Haze:
            return 'Haze'
        case MeldMethod.Moonbow:
            return 'Moonbow'
        case MeldMethod.Wisp:
            return 'Wisp of Mystery'
        case MeldMethod.Rebirth:
            return 'Rebirth'
        case MeldMethod.Anima:
            return 'Anima'
        case MeldMethod.Reincarnation:
            return 'Reincarnation'
    }
}

function oddsToString(
    method: MeldMethod,
    odds: BigAmount,
    points: number,
    subpoints?: number
) {
    let line = points + (points === 1 ? ' point ' : ' points')
    if (subpoints !== undefined) {
        line +=
            ', ' + subpoints + (subpoints === 1 ? ' subpoint ' : ' subpoints')
    }
    line += ' or better: '
    let oneIn =
        odds.num === 0n
            ? 'infinity'
            : odds.inv().quantize(1n, 'CEIL').num.toString()
    line += '1-in-' + oneIn + ', '
    let maxInt = BigInt(Number.MAX_SAFE_INTEGER)
    let safeRatio = odds
    if (odds.num > maxInt || odds.den > maxInt) {
        const numFactor = odds.num / maxInt
        const denFactor = odds.den / maxInt
        const factor = numFactor > denFactor ? numFactor : denFactor
        safeRatio = q(odds.num / factor, odds.den / factor)
    }
    let approx = safeRatio.toNumber()
    let medianCharmsNeeded = Math.log(0.5) / Math.log(1 - approx)
    let quests = Math.ceil(
        medianCharmsNeeded / (method === MeldMethod.Wisp ? 5 : 3)
    )
    line += quests + ' quests for 50% chance, '
    let percent = (approx * 100).toFixed(5)
    line += percent + '% (exact value: ' + odds.num + '/' + odds.den + ')'
    return line
}

const slotRates: number[][][][][] =
    // method, slot num, grade 1, grade 2, level
    [
        // Reflecting Pool
        [
            // Slot 1
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
        ],
        // Haze
        [
            // Slot 1
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [70, 30, 0, 0], // B
                    [50, 40, 10, 0], // C
                    [40, 40, 20, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [50, 30, 20, 0], // B
                    [30, 40, 20, 10], // C
                    [20, 50, 20, 10], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
        ],
        // Moonbow
        [
            // Slot 1
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [80, 20, 0, 0], // B
                    [60, 40, 0, 0], // C
                    [40, 40, 20, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [50, 30, 20, 0], // B
                    [30, 40, 20, 10], // C
                    [20, 50, 20, 10], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [60, 40, 0, 0], // A
                    [30, 40, 20, 10], // B
                    [10, 40, 40, 10], // C
                    [0, 40, 40, 20], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [80, 20, 0, 0], // B
                    [80, 20, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [80, 20, 0, 0], // B
                    [70, 20, 10, 0], // C
                    [70, 20, 10, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [70, 20, 10, 0], // B
                    [40, 50, 10, 0], // C
                    [40, 40, 20, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // A
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // B
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
                // C
                [
                    [100, 0, 0, 0], // S
                    [100, 0, 0, 0], // A
                    [100, 0, 0, 0], // B
                    [100, 0, 0, 0], // C
                    [100, 0, 0, 0], // None
                ],
            ],
        ],
        // Wisp of Mystery
        [
            // Slot 1
            [
                // S
                [
                    [80, 15, 3, 2], // S
                    [80, 15, 3, 2], // A
                    [70, 23, 5, 2], // B
                    [70, 23, 5, 2], // C
                    [50, 40, 5, 5], // None
                ],
                // A
                [
                    [80, 15, 3, 2], // S
                    [60, 30, 5, 5], // A
                    [50, 40, 5, 5], // B
                    [50, 40, 5, 5], // C
                    [40, 50, 5, 5], // None
                ],
                // B
                [
                    [70, 23, 5, 2], // S
                    [50, 40, 5, 5], // A
                    [20, 50, 20, 10], // B
                    [20, 50, 20, 10], // C
                    [20, 50, 20, 10], // None
                ],
                // C
                [
                    [70, 23, 5, 2], // S
                    [50, 40, 5, 5], // A
                    [20, 50, 20, 10], // B
                    [0, 40, 30, 30], // C
                    [0, 40, 30, 30], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // A
                [
                    [90, 10, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [70, 30, 0, 0], // B
                    [70, 30, 0, 0], // C
                    [60, 40, 0, 0], // None
                ],
                // B
                [
                    [90, 10, 0, 0], // S
                    [70, 30, 0, 0], // A
                    [50, 40, 10, 0], // B
                    [50, 40, 10, 0], // C
                    [50, 40, 10, 0], // None
                ],
                // C
                [
                    [90, 10, 0, 0], // S
                    [70, 30, 0, 0], // A
                    [50, 40, 10, 0], // B
                    [50, 40, 10, 0], // C
                    [50, 40, 10, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // A
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // B
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // C
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
            ],
        ],
        // Rebirth
        [
            // Slot 1
            [
                // S
                [
                    [80, 15, 3, 2], // S
                    [80, 15, 3, 2], // A
                    [70, 23, 5, 2], // B
                    [70, 23, 5, 2], // C
                    [50, 40, 5, 5], // None
                ],
                // A
                [
                    [80, 15, 3, 2], // S
                    [60, 30, 5, 5], // A
                    [50, 40, 5, 5], // B
                    [50, 40, 5, 5], // C
                    [40, 50, 5, 5], // None
                ],
                // B
                [
                    [70, 23, 5, 2], // S
                    [50, 40, 5, 5], // A
                    [20, 50, 20, 10], // B
                    [20, 50, 20, 10], // C
                    [20, 50, 20, 10], // None
                ],
                // C
                [
                    [70, 23, 5, 2], // S
                    [50, 40, 5, 5], // A
                    [20, 50, 20, 10], // B
                    [0, 40, 30, 30], // C
                    [0, 40, 30, 30], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // A
                [
                    [90, 10, 0, 0], // S
                    [80, 20, 0, 0], // A
                    [70, 30, 0, 0], // B
                    [70, 30, 0, 0], // C
                    [60, 40, 0, 0], // None
                ],
                // B
                [
                    [90, 10, 0, 0], // S
                    [70, 30, 0, 0], // A
                    [50, 40, 10, 0], // B
                    [50, 40, 10, 0], // C
                    [50, 40, 10, 0], // None
                ],
                // C
                [
                    [90, 10, 0, 0], // S
                    [70, 30, 0, 0], // A
                    [50, 40, 10, 0], // B
                    [50, 40, 10, 0], // C
                    [50, 40, 10, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // A
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // B
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
                // C
                [
                    [90, 10, 0, 0], // S
                    [90, 10, 0, 0], // A
                    [90, 10, 0, 0], // B
                    [90, 10, 0, 0], // C
                    [80, 20, 0, 0], // None
                ],
            ],
        ],
        // Anima
        [
            // Slot 1
            [
                // S
                [
                    [75, 15, 5, 3, 2], // S
                    [75, 15, 5, 3, 2], // A
                    [65, 23, 7, 3, 2], // B
                    [65, 23, 7, 3, 2], // C
                    [43, 40, 7, 7, 3], // None
                ],
                // A
                [
                    [76, 15, 4, 3, 2], // S
                    [53, 30, 7, 7, 3], // A
                    [43, 40, 7, 7, 3], // B
                    [43, 40, 7, 7, 3], // C
                    [32, 50, 7, 7, 4], // None
                ],
                // B
                [
                    [65, 23, 7, 3, 2], // S
                    [45, 37, 7, 7, 4], // A
                    [20, 44, 20, 10, 6], // B
                    [20, 43, 20, 10, 7], // C
                    [10, 40, 25, 15, 10], // None
                ],
                // C
                [
                    [64, 25, 6, 3, 2], // S
                    [40, 40, 8, 7, 5], // A
                    [8, 45, 25, 15, 7], // B
                    [0, 25, 30, 30, 15], // C
                    [0, 20, 30, 30, 20], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // A
                [
                    [80, 20, 0, 0, 0], // S
                    [70, 30, 0, 0, 0], // A
                    [60, 40, 0, 0, 0], // B
                    [60, 40, 0, 0, 0], // C
                    [50, 50, 0, 0, 0], // None
                ],
                // B
                [
                    [80, 20, 0, 0, 0], // S
                    [60, 40, 0, 0, 0], // A
                    [40, 40, 20, 0, 0], // B
                    [40, 40, 20, 0, 0], // C
                    [40, 30, 30, 0, 0], // None
                ],
                // C
                [
                    [80, 20, 0, 0, 0], // S
                    [60, 40, 0, 0, 0], // A
                    [40, 40, 20, 0, 0], // B
                    [40, 40, 20, 0, 0], // C
                    [40, 30, 30, 0, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // A
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // B
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // C
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
            ],
        ],
        // Reincarnation
        [
            // Slot 1
            [
                // S
                [
                    [75, 15, 5, 3, 2], // S
                    [75, 15, 3, 2, 2], // A
                    [65, 23, 7, 3, 2], // B
                    [65, 23, 7, 3, 2], // C
                    [43, 40, 7, 7, 3], // None
                ],
                // A
                [
                    [76, 15, 4, 3, 2], // S
                    [53, 30, 7, 7, 3], // A
                    [43, 40, 7, 7, 3], // B
                    [43, 40, 7, 7, 3], // C
                    [32, 50, 7, 7, 4], // None
                ],
                // B
                [
                    [65, 23, 7, 3, 2], // S
                    [45, 37, 7, 7, 4], // A
                    [20, 44, 20, 10, 6], // B
                    [20, 43, 20, 10, 7], // C
                    [10, 40, 25, 15, 10], // None
                ],
                // C
                [
                    [64, 25, 6, 3, 2], // S
                    [40, 40, 8, 7, 5], // A
                    [8, 45, 25, 15, 7], // B
                    [0, 25, 30, 30, 15], // C
                    [0, 20, 30, 30, 20], // None
                ],
            ],
            // Slot 2
            [
                // S
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // A
                [
                    [80, 20, 0, 0, 0], // S
                    [70, 30, 0, 0, 0], // A
                    [60, 40, 0, 0, 0], // B
                    [60, 40, 0, 0, 0], // C
                    [50, 50, 0, 0, 0], // None
                ],
                // B
                [
                    [80, 20, 0, 0, 0], // S
                    [60, 40, 0, 0, 0], // A
                    [40, 40, 20, 0, 0], // B
                    [40, 40, 20, 0, 0], // C
                    [40, 30, 30, 0, 0], // None
                ],
                // C
                [
                    [80, 20, 0, 0, 0], // S
                    [60, 40, 0, 0, 0], // A
                    [40, 40, 20, 0, 0], // B
                    [40, 40, 20, 0, 0], // C
                    [40, 30, 30, 0, 0], // None
                ],
            ],
            // Slot 3
            [
                // S
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // A
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // B
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
                // C
                [
                    [80, 20, 0, 0, 0], // S
                    [80, 20, 0, 0, 0], // A
                    [80, 20, 0, 0, 0], // B
                    [80, 20, 0, 0, 0], // C
                    [70, 30, 0, 0, 0], // None
                ],
            ],
        ],
    ]

function slotOdds(
    slot: number,
    skill1: Skill,
    skill2: Skill,
    method: MeldMethod
): BigAmount[] {
    const gradeToIndex = (grade: Grade) => {
        return 3 - grade
    }
    const gIdx1 = gradeToIndex(skill1.grade)
    const gIdx2 = gradeToIndex(skill2.grade)
    return slotRates[method][slot - 1][gIdx1][gIdx2].map((level) =>
        q(level, 100)
    )
}

const grades: Grade[] = [Grade.C, Grade.B, Grade.A, Grade.S]
const allSkills: Skill[] = AllSkills
const skillsByGrade: Skill[][] = grades.map((grade) =>
    allSkills.filter((skill) => skill.grade === grade)
)
const skillGradeRates: BigAmount[][][] = [
    // 5 meld methods, 3 tables, 4 grades
    [
        [0, 0, 0, 0],
        [100, 0, 0, 0],
        [100, 0, 0, 0],
    ],
    [
        [0, 0, 0, 0],
        [100, 0, 0, 0],
        [60, 25, 15, 0],
    ],
    [
        [0, 0, 0, 0],
        [50, 50, 0, 0],
        [60, 30, 10, 0],
    ],
    [
        [30, 52, 15, 3],
        [0, 0, 0, 0],
        [30, 52, 15, 3],
    ],
    [
        [25, 50, 20, 5],
        [0, 0, 0, 0],
        [25, 50, 20, 5],
    ],
    [
        [20, 49, 23, 8],
        [0, 0, 0, 0],
        [20, 49, 23, 8],
    ],
    [
        [20, 49, 23, 8],
        [0, 0, 0, 0],
        [20, 49, 23, 8],
    ],
].map((method) => method.map((table) => table.map((grade) => q(grade, 100))))
const rateOfSecondSkill: BigAmount[][] = [
    // 5 meld methods, 4 grades for first skill
    [0, 0, 0, 0],
    [60, 20, 0, 0],
    [80, 60, 30, 0],
    [100, 100, 40, 20],
    [100, 100, 20, 10],
    [100, 100, 30, 20],
    [100, 100, 30, 20],
].map((method) => method.map((grade) => q(grade, 100)))
