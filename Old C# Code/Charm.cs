﻿using System.Linq;
using Numerics;

namespace MHRCharmOdds
{
    public class Charm
    {
        public Skill Skill1 { get; }
        public int Level1 { get; }
        public Skill Skill2 { get; }
        public int Level2 { get; }
        public int[] Slots { get; }

        public Charm(Skill skill1, int level1, Skill skill2, int level2, int[] slots)
        {
            this.Skill1 = skill1;
            this.Level1 = level1;
            this.Skill2 = skill2;
            this.Level2 = level2;
            this.Slots = slots;
        }

        public int Points()
        {
            return (this.Skill1.Slot > 1 ? this.Level1 : 0) + (this.Skill2.Slot > 1 ? this.Level2 : 0) + this.Slots.Count(x => x > 1);
        }

        public int SubPoints()
        {
            return (this.Skill1.Slot == 1 ? this.Level1 : 0) + (this.Skill2.Slot == 1 ? this.Level2 : 0) + this.Slots.Count(x => x == 1);
        }

        public override string ToString()
        {
            return $"{this.Skill1.Name}{this.Level1}/{this.Skill2.Name}{this.Level2} {this.Slots.Aggregate("", (s, x) => s + "-" + x)}";
        }

        public override bool Equals(object obj)
        {
            Charm other = obj as Charm;
            return ((this.Skill1 == other.Skill1 &&
                this.Level1 == other.Level1 &&
                this.Skill2 == other.Skill2 &&
                this.Level2 == other.Level2) ||
                (this.Skill1 == other.Skill2 &&
                this.Level1 == other.Level2 &&
                this.Skill2 == other.Skill1 &&
                this.Level2 == other.Level1)) &&
                this.Slots.OrderByDescending(x => x).Zip(other.Slots.OrderByDescending(x => x), (x, y) => x == y).All(x => x);
        }

        public override int GetHashCode()
        {
            return this.Skill1.GetHashCode() * this.Skill2.GetHashCode() +
                2 * this.Level1 * this.Level2 +
                3 * this.Slots.Sum();
        }
    }

    public class CharmRoll
    {
        public Charm Charm { get; }
        public BigRational Odds { get; }

        public CharmRoll(Charm charm, BigRational odds)
        {
            this.Charm = charm;
            this.Odds = odds;
        }
    }
}
