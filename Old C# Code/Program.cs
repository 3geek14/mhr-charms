﻿using Numerics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MHRCharmOdds
{
    public class Program
    {
        // TODO: Account for charms where skill #1 isn't worth points, but either skill #2 is or there are slots.
        // TODO: Account for different numbers of available skills for Wisp/Rebirth and for first/second skill.
        public static void Main(string[] args)
        {
            MeldMethod method = MeldMethod.Wisp;

            BigRational[] skillGradeRatesWisp = new int[] { 30, 52, 15, 3 }.Select(x => new BigRational(x, 100)).ToArray();
            BigRational[] skillGradeRatesRebirth = new int[] { 25, 50, 20, 5 }.Select(x => new BigRational(x, 100)).ToArray();

            Skill attackBoost = new Skill("AB", Grade.A, 2, new int[] { 60, 30, 10 }, new int[] { 90, 10 });
            Skill criticalEye = new Skill("CE", Grade.A, 2, new int[] { 60, 30, 10 }, new int[] { 90, 10 });
            Skill criticalBoost = new Skill("CB", Grade.S, 2, new int[] { 95, 5 }, new int[] { 100 });
            Skill weaknessExploit = new Skill("WEX", Grade.A, 2, new int[] { 60, 40 }, new int[] { 90, 10 });
            Skill handicraft = new Skill("Handicraft", Grade.S, 3, new int[] { 95, 5 }, new int[] { 100 });
            Skill hornMaestro = new Skill("Maestro", Grade.C, 1, new int[] { 100 }, new int[] { 100 });
            Skill speedSharpening = new Skill("SS", Grade.B, 1, new int[] { 50, 45, 5 }, new int[] { 75, 25 });
            Skill wirebugWhisperer = new Skill("WW", Grade.C, 2, new int[] { 90, 10, 5 }, new int[] { 50, 45, 5 });
            Skill evadeExtender = new Skill("EE", Grade.B, 2, new int[] { 50, 45, 5 }, new int[] { 75, 25 });

            Skill[] skills;
            //skills = new Skill[] { attackBoost, criticalEye, criticalBoost, weaknessExploit, handicraft, hornMaestro, speedSharpening/*, wirebugWhisperer*/ };
            //skills = new Skill[] { attackBoost, criticalBoost, weaknessExploit };
            skills = new Skill[] { criticalBoost, weaknessExploit, evadeExtender, attackBoost, criticalEye };
            Skill[] skillsOrNull = skills.Append(Skill.Null).ToArray();

            int numGradeC = 29;
            int numGradeB = 30;
            int numGradeA = 26;
            int numGradeS = 16;
            int[] numPerGrade = new int[] { numGradeC, numGradeB, numGradeA, numGradeS };

            BigRational[] rateOfSecondSkillWisp = new int[] { 100, 100, 40, 20 }.Select(x => new BigRational(x, 100)).ToArray();
            BigRational[] rateOfSecondSkillRebirth = new int[] { 100, 100, 20, 10 }.Select(x => new BigRational(x, 100)).ToArray();

            Console.WriteLine($"Rates for {(method == MeldMethod.Wisp ? "Wisp of Mystery" : "Rebirth")}:");
            List<CharmRoll> charms = new List<CharmRoll>();
            foreach (var skill1 in skills)
            {
                int grade1Index = (int)skill1.Grade - 1;
                BigRational chanceOfTwoSkills = method == MeldMethod.Wisp ? rateOfSecondSkillWisp[grade1Index] : rateOfSecondSkillRebirth[grade1Index];

                BigRational firstGrade = method == MeldMethod.Wisp ? skillGradeRatesWisp[grade1Index] : skillGradeRatesRebirth[grade1Index];
                BigRational firstSkillFromGrade = new BigRational(1, numPerGrade[grade1Index]);
                for (int skill1Level = 1; skill1Level <= skill1.Skill1Odds.Length; ++skill1Level)
                {
                    BigRational firstLevel = skill1.Skill1Odds[skill1Level - 1];
                    foreach (var skill2 in skillsOrNull)
                    {
                        if (skill1.Equals(skill2)) continue;
                        int grade2Index = (int)skill2.Grade - 1;

                        BigRational secondGrade = skill2.Equals(Skill.Null) ? (1 - chanceOfTwoSkills) : (chanceOfTwoSkills * (method == MeldMethod.Wisp ? skillGradeRatesWisp[grade2Index] : skillGradeRatesRebirth[grade2Index]));
                        BigRational secondSkillFromGrade = skill2.Equals(Skill.Null) ? 1 : new BigRational(1, numPerGrade[grade2Index] - 1);
                        for (int skill2Level = 1; skill2Level <= skill2.Skill2Odds.Length; ++skill2Level)
                        {
                            BigRational secondLevel = skill2.Skill2Odds[skill2Level - 1];
                            BigRational[] slot1Odds = Slot1Odds(skill1, skill2);
                            BigRational[] slot2Odds = Slot2Odds(skill1, skill2);
                            BigRational[] slot3Odds = Slot3Odds(skill1, skill2);
                            for (int slot1Level = 0; slot1Level < slot1Odds.Length; ++slot1Level)
                            {
                                BigRational slot1 = slot1Odds[slot1Level];
                                for (int slot2Level = 0; slot2Level < slot2Odds.Length; ++slot2Level)
                                {
                                    BigRational slot2 = slot2Odds[slot2Level];
                                    for (int slot3Level = 0; slot3Level < slot3Odds.Length; ++slot3Level)
                                    {
                                        BigRational slot3 = slot3Odds[slot3Level];
                                        int[] slots = new int[] { slot1Level, slot2Level, slot3Level }.Where(x => x > 0).ToArray();
                                        Charm charm = new Charm(skill1, skill1Level, skill2, skill2Level, slots);
                                        BigRational chance = firstGrade * firstSkillFromGrade * firstLevel * secondGrade * secondSkillFromGrade * secondLevel * slot1 * slot2 * slot3;
                                        charms.Add(new CharmRoll(charm, chance));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var subpts = charms
                .GroupBy(charm => charm.Charm.Points())
                .SelectMany(point => point
                    .GroupBy(charm => charm.Charm.SubPoints())
                    .Select(subPoint => (point: point.Key, subPoint: subPoint.Key, odds: subPoint.Aggregate(BigRational.Zero, (sum, charm) => sum + charm.Odds))))
                .OrderBy(x => x.point)
                .ThenBy(x => x.subPoint)
                .ToList();
            var pts = charms
                .GroupBy(charm => charm.Charm.Points())
                .Select(point=>(point: point.Key, odds: point.Aggregate(BigRational.Zero, (sum, charm) => sum + charm.Odds)))
                .OrderBy(x => x.point)
                .ToList();
            var subptsOrBetter = subpts
                .Select((group, i) => (group.subPoint, group.subPoint, odds: subpts.Skip(i).Aggregate(BigRational.Zero, (sum, g) => sum + g.odds)));
            var ptsOrBetter = pts
                .Select((group, i) => (group.point, odds: pts.Skip(i).Aggregate(BigRational.Zero, (sum, g) => sum + g.odds)));
            Console.WriteLine("Odds of specific points/subpoints:");
            foreach ((int point, int subPoint, BigRational odds) in subpts)
            {
                Console.WriteLine($"{point} points, {subPoint} subpoints: {odds}");
            }
            Console.WriteLine("Odds of points/subpoints or better:");
            foreach ((int point, int subPoint, BigRational odds) in subptsOrBetter)
            {
                Console.WriteLine($"{point} points, {subPoint} subpoints: {odds}");
            }
            Console.WriteLine("Odds of points or better:");
            foreach ((int point, BigRational odds) in ptsOrBetter)
            {
                Console.WriteLine($"{point} points: {odds}");
            }
        }

        public static BigRational[] Slot1Odds(Skill skill1, Skill skill2)
        {
            Grade higher = (Grade)Math.Max((byte)skill1.Grade, (byte)skill2.Grade);
            Grade lower = (Grade)Math.Min((byte)skill1.Grade, (byte)skill2.Grade);
            int[] odds = new int[0];
            switch (higher)
            {
                case Grade.S:
                    switch (lower)
                    {
                        case Grade.S:
                            odds = new int[] { 80, 15, 3, 2 };
                            break;
                        case Grade.A:
                            odds = new int[] { 80, 15, 3, 2 };
                            break;
                        case Grade.B:
                            odds = new int[] { 70, 23, 5, 2 };
                            break;
                        case Grade.C:
                            odds = new int[] { 70, 23, 5, 2 };
                            break;
                        default:
                            odds = new int[] { 50, 40, 5, 5 };
                            break;
                    }
                    break;
                case Grade.A:
                    switch (lower)
                    {
                        case Grade.A:
                            odds = new int[] { 60, 30, 5, 5 };
                            break;
                        case Grade.B:
                            odds = new int[] { 50, 40, 5, 5 };
                            break;
                        case Grade.C:
                            odds = new int[] { 50, 40, 5, 5 };
                            break;
                        default:
                            odds = new int[] { 40, 50, 5, 5 };
                            break;
                    }
                    break;
                case Grade.B:
                    switch (lower)
                    {
                        case Grade.B:
                            odds = new int[] { 20, 50, 20, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 20, 50, 20, 10 };
                            break;
                        default:
                            odds = new int[] { 20, 50, 20, 10 };
                            break;
                    }
                    break;
                case Grade.C:
                    switch (lower)
                    {
                        case Grade.C:
                            odds = new int[] { 0, 40, 30, 30 };
                            break;
                        default:
                            odds = new int[] { 0, 40, 30, 30 };
                            break;
                    }
                    break;
            }
            return odds.Select(x => new BigRational(x, odds.Sum())).ToArray();
        }

        public static BigRational[] Slot2Odds(Skill skill1, Skill skill2)
        {
            Grade higher = (Grade)Math.Max((byte)skill1.Grade, (byte)skill2.Grade);
            Grade lower = (Grade)Math.Min((byte)skill1.Grade, (byte)skill2.Grade);
            int[] odds = new int[0];
            switch (higher)
            {
                case Grade.S:
                    switch (lower)
                    {
                        case Grade.S:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.A:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.B:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 90, 10 };
                            break;
                        default:
                            odds = new int[] { 80, 20 };
                            break;
                    }
                    break;
                case Grade.A:
                    switch (lower)
                    {
                        case Grade.A:
                            odds = new int[] { 80, 20 };
                            break;
                        case Grade.B:
                            odds = new int[] { 70, 30 };
                            break;
                        case Grade.C:
                            odds = new int[] { 70, 30 };
                            break;
                        default:
                            odds = new int[] { 60, 40 };
                            break;
                    }
                    break;
                case Grade.B:
                    switch (lower)
                    {
                        case Grade.B:
                            odds = new int[] { 50, 40, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 50, 40, 10 };
                            break;
                        default:
                            odds = new int[] { 50, 40, 10 };
                            break;
                    }
                    break;
                case Grade.C:
                    switch (lower)
                    {
                        case Grade.C:
                            odds = new int[] { 50, 40, 10 };
                            break;
                        default:
                            odds = new int[] { 50, 40, 10 };
                            break;
                    }
                    break;
            }
            return odds.Select(x => new BigRational(x, odds.Sum())).ToArray();
        }

        public static BigRational[] Slot3Odds(Skill skill1, Skill skill2)
        {
            Grade higher = (Grade)Math.Max((byte)skill1.Grade, (byte)skill2.Grade);
            Grade lower = (Grade)Math.Min((byte)skill1.Grade, (byte)skill2.Grade);
            int[] odds = new int[0];
            switch (higher)
            {
                case Grade.S:
                    switch (lower)
                    {
                        case Grade.S:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.A:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.B:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 90, 10 };
                            break;
                        default:
                            odds = new int[] { 80, 20 };
                            break;
                    }
                    break;
                case Grade.A:
                    switch (lower)
                    {
                        case Grade.A:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.B:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 90, 10 };
                            break;
                        default:
                            odds = new int[] { 80, 20 };
                            break;
                    }
                    break;
                case Grade.B:
                    switch (lower)
                    {
                        case Grade.B:
                            odds = new int[] { 90, 10 };
                            break;
                        case Grade.C:
                            odds = new int[] { 90, 10 };
                            break;
                        default:
                            odds = new int[] { 80, 20 };
                            break;
                    }
                    break;
                case Grade.C:
                    switch (lower)
                    {
                        case Grade.C:
                            odds = new int[] { 90, 10 };
                            break;
                        default:
                            odds = new int[] { 80, 20 };
                            break;
                    }
                    break;
            }
            return odds.Select(x => new BigRational(x, odds.Sum())).ToArray();
        }
    }

    public enum MeldMethod
    {
        Wisp, Rebirth
    }
}
