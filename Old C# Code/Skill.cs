﻿using System.Linq;
using Numerics;

namespace MHRCharmOdds
{
    public class Skill
    {
        public static Skill Null = new Skill("[null]", Grade.Null, 0, new int[] { 100 }, new int[] { 100 });

        public string Name { get; }
        public Grade Grade { get; }
        public int Slot { get; }
        public BigRational[] Skill1Odds { get; }
        public BigRational[] Skill2Odds { get; }

        public Skill(string name, Grade grade, int slot, int[] skill1Odds, int[] skill2Odds)
        {
            this.Name = name;
            this.Grade = grade;
            this.Slot = slot;
            this.Skill1Odds = skill1Odds.Select(x => new BigRational(x, skill1Odds.Sum())).ToArray();
            this.Skill2Odds = skill2Odds.Select(x => new BigRational(x, skill2Odds.Sum())).ToArray();
        }

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            Skill other = obj as Skill;
            return this.Name == other.Name;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }

    public enum Grade
    {
        Null, C, B, A, S
    }
}
